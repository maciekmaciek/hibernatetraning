package connections;

import domain.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class DataConnection {

    public static void entityAddNewObjectToDataAndCreateConnection() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("learnhibernate2");
        EntityManager entityManager = factory.createEntityManager();
        Student tempStudent = new Student("Adam", "Tellman", "adam@lu2code.com");
        entityManager.getTransaction().begin();
        entityManager.persist(tempStudent);
        entityManager.getTransaction().commit();

    }

    public static void sessionAddNewObjectToDataAndCreateConnection() {
        SessionFactory factory = new Configuration()
                .configure("META-INF/hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();
        Session session = factory.getCurrentSession();
        try {
            Student tempStudent = new Student("Paul", "Wall", "paul@lu2code.com");
            session.beginTransaction();
            session.save(tempStudent);
            session.getTransaction().commit();
        } finally {
            factory.close();
        }
    }

    public static void entityReadObjectFromData() {
        SessionFactory factory = new Configuration()
                .configure("META-INF/hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();
        Session session = factory.getCurrentSession();

        try {
            session.beginTransaction();
            List<Student> getStudent = session.createQuery("from Student")
                    .list();
            displayStudent(getStudent);
            System.out.println("***********************");
            getStudent = session.createQuery("from Student stu where stu.lastName='Tellman'").getResultList();
            System.out.println("***********************");
            displayStudent(getStudent);
            session.getTransaction().commit();

        } finally {
            factory.close();

        }

    }

    private static void displayStudent(List<Student> getStudent) {
        for (Student students : getStudent) {
            System.out.println(students);
        }
    }

}
